ENV['RAILS_ENV'] ||= 'test'

class ActiveSupport::TestCase
  require_relative "../test_helper"
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
end
